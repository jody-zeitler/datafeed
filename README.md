# Flywheel Data Feed

Frontend Candidate Project

## Purpose

The purpose of this project is to provide a skeleton app and requirements in assessing the skill set of candidates applying for a frontend position at Flywheel. The project is intended to be free-form outside of the defined requirements insofar as styling and architecture. Candidates could even choose to forego this Angular skeleton app in favor of another platform such as React or Vue.

The deliverable for this project is a set of components and services that interact with the Flywheel API to display recent data within the platform.

## Background

One of the facets of the Flywheel platform is the organization of data into a hierarchy of containers that represent different resolutions of a research project. Group and Project are at the top of the structure, and they are further broken down into Subjects, Sessions, and Acquisitions. The data for a project is collected as files within an acquisition.

```
group
├── project
│   ├── subject
│   │   ├── session
│   │   └── session
│   │       └── acquisition
│   │           ├── file
│   │           └── file
│   └── subject
```

The UI at https://candidate.staging.flywheel.io, which you can log into with provided credentials, displays this hierarchy in a top-down representation, starting at either the project or the session level. The task for this project is to present project data in a bottom-up manner, showing the most-recent acquisitions as a feed or activity stream, along with a sample of their files. For the sake of simplicity and amusement, the data you'll be working with in these acquisitions are an assortment of images from https://placedog.net.

This Angular skeleton app provides a login interface that sets an authentication token for the Flywheel API, and passes this token along for any requests made by the HttpClient. It also sets up an authorization guard for the `/` route where your page will render. A dev server proxy to the `candidate.staging` instance is set in the `src/proxy.conf.json` file, via the `/api` path. Your user is granted `read-write` access to a project, in which you are able to interact with the data as you see fit.

Documentation for the Flywheel API is located at https://flywheel-io.gitlab.io/product/backend/sdk/branches/master/swagger/index.html.

## Requirements

![mockup](doc/mockup-00.png)

1. Acquisitions are rendered in a list.
    1. Acquisitions are sorted by `timestamp` date descending.
    1. Only 20 acquisitions are shown on the page.
2. Each acquisition is represented by a card.
    1. The card shows:
        1. The acquisition label
        1. The `timestamp` of the acquisition
        1. The number of files within the acquisition
    1. Clicking on an acquisition card renders its expanded view. (See #3)
3. A selected acquisition card renders its expanded view.
    1. In addition to the collapsed information, the expanded card shows:
        1. The parent session, subject, and project labels
        1. The thumbnails for the first 5 images within the acquisition
    1. Clicking on an image thumbnail renders the full image in a new tab.
4. The first acquisition is selected/expanded when the page loads.

## Procedure

1. Clone this repository
1. Install the Angular CLI with `npm i -g @angular/cli`
1. Start the app with `npm start`
1. Use the provided username and password to log into the app at http://localhost:4200
1. Make your changes to the application
1. Pack up the application with `npm pack` to produce `datafeed-0.0.0.tgz`
1. Send `datafeed-0.0.0.tgz` back to your contact at Flywheel.

## Additional Information

You can request all acquisitions that your user can see from `/api/acquisitions`, you're able to limit and sort with query parameters `?limit=20&sort=timestamp:desc`. These parameters apply to all list endpoints in the API. There is also an expanded pagination response that gets sent if you set the header `x-accept-feature: pagination`.

The parent IDs for an acquisition (or any container) are available in the `parents` object:
```
"parents": {
  "group": "datafeed",
  "project": "5f29c3a4a107ee0c86b31cbc",
  "subject": "5f46e2e88bf91dd87b828d66",
  "session": "5f4947771051fa46e32e0b8b"
}
```

In order to get a single container, you can use the specialized endpoints (`/api/sessions/{id}`, `/api/subjects/{id}`) or the generic container endpoint `/api/containers/{id}`. The response is the same.

### File Retrieval

Because we use an authorization header instead of cookies, getting image thumbnails and data may be tricky. The standard approach is to request a file ticket that can be placed as a query parameter for the image URL. This is done by specifying an empty `ticket=` query parameter. Set the ticket value you get in the request to get the image data. You should also specify a `view=true` parameter to set the MIME type correctly.

```
GET /api/containers/5f4947771051fa46e32e0b8f/files/placedog_00.jpg?ticket=
{"ticket": "ab514fca-6690-44ba-91ad-bb6487c299f0"}

GET /api/containers/5f4947771051fa46e32e0b8f/files/placedog_00.jpg?ticket=ab514fca-6690-44ba-91ad-bb6487c299f0&view=true
<image data>
```

Alternatively, you could request image data via XHR and set the image element source as a data URL.

----

## CLI Guide

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.8.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
