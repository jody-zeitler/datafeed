import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { UserService } from '../../user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  form = new FormGroup({
    username: new FormControl('', [ Validators.required ]),
    password: new FormControl('', [ Validators.required ]),
  })

  private redirectUrl: string

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
  ) {
    this.redirectUrl = this.route.snapshot.queryParams.redirectUrl;
  }

  isFieldInvalid(key: string): boolean {
    const control = this.form.get(key);
    return control.touched && control.invalid;
  }

  login(): void {
    if (this.form.invalid) {
      return;
    }
    const { username, password } = this.form.value;
    this.userService.basicLogin(username, password).subscribe({
      next: () => {
        this.router.navigateByUrl(this.redirectUrl || '');
      }
    });
  }
}
