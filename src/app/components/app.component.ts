import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { User } from '../models/user.model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private router: Router,
    private userService: UserService,
  ) { }

  get user$(): Observable<User> {
    return this.userService.user$;
  }

  logout(): void {
    this.userService.logout();
    this.router.navigate(['login'], { queryParams: { redirectUrl: this.router.url } });
  }
}
