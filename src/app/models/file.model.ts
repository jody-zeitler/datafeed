export interface File {
  _id: string
  created: string
  mimetype: string
  modified: string
  name: string
  size: number
  type: string
}
