import { File } from './file.model';

export interface Acquisition {
  _id: string
  created: string
  files: File[]
  label: string
  modified: string
  parents: {
    group: string
    project: string
    session: string
    subject: string
  }
  timestamp: string
}
