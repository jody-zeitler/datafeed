import { HttpClient, HttpBackend } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError, ReplaySubject } from 'rxjs';
import { switchMap, tap, catchError } from 'rxjs/operators';

import { User } from './models/user.model';

const TOKEN_STORAGE_KEY = 'flywheel_token';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  readonly user$ = new ReplaySubject<User>(1)

  private http: HttpClient

  constructor(backend: HttpBackend) {
    this.http = new HttpClient(backend);
    if (this.token) {
      this.refreshUser().subscribe(user => {
        this.user$.next(user);
      });
    } else {
      this.user$.next(null);
    }
  }

  get token(): string {
    return localStorage.getItem(TOKEN_STORAGE_KEY);
  }

  set token(value: string) {
    if (value) {
      localStorage.setItem(TOKEN_STORAGE_KEY, value);
    } else {
      localStorage.removeItem(TOKEN_STORAGE_KEY);
    }
  }

  private refreshUser(): Observable<User> {
    return this.http.get<User>('/api/users/self', { headers: { authorization: this.token } }).pipe(
      tap(user => {
        this.user$.next(user);
      }),
      catchError(error => {
        this.logout();
        return throwError(error);
      }),
    );
  }

  basicLogin(username: string, password: string): Observable<User> {
    return this.http.post<{token: string}>('/api/login/basic', {
      email: username,
      password: password,
    }).pipe(
      switchMap(({token}) => {
        this.token = token;
        return this.refreshUser();
      }),
    );
  }

  logout(): void {
    this.token = null;
    this.user$.next(null);
  }
}
